+++
date = "2017-04-05T15:31:00-05:00"
title = "Hugo - First Steps"
topics = [ "hugo" ]
subtopic = "intro"

+++

https://gitlab.com/pages/hugo

hugo new site mysite

cd mysite

hugo new post/sample-post.md

file will be created in content/post/sample-post.md

Add some content to sample-post.md:

    # A post

    This is my post!


refresh browser, nothing has changed.

start with two concepts:
- content
- template

example of content:
    - post/_index.md
    - post/sample-post.md

(http://gohugo.io/content/using-index-md/)

about index.md:


_index.md location                  Page affected               Rendered by
/content/post/_index.md             site.com/post/              /layouts/section/post.html
/content/categories/hugo/_index.md  site.com/categories/hugo/   /layouts/taxonomy/hugo.html

