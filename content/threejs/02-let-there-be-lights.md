+++
date = "2017-03-30T19:02:45-05:00"
title = "02 - Let There Be Lights"
topics = [ "threejs" ]
subtopic = "basics"

+++

## Shine the light

The first next thing to do will be to add some light and colour to our [wireframe world]({{< absURL "/assets/threejs/basic" >}}).

What lights do in threejs scene is that they are interacting with materials assigned to meshes.
If a mesh had no material it would be invisible. So, basic requirement for lights to work in our threejs world
is to have at least one mesh which has a material assigned to it, a material that will nicely reflect our light(s)
off of it.

As usual, there are different kinds of lights we could use. Since we are concerned with two things in these tutorials:

- concepts
- simplicity

we will use simple [`AmbientLight`](https://threejs.org/docs/?q=light#Reference/Lights/AmbientLight) for our first light. This light globally illuminates all the objects in the scene. It has no direction or a specific point where it comes from, so it cannot cast shadows. On the other hand it is very simple and straghtforward to set up and use, and it will reveal all the objects in our scene.

Let's create one:

```javascript
// rgbColor, intensity
var light = new THREE.AmbientLight( 0x404040 ); // intensity is max if we leave it out
scene.add( light );
```

and that's it!

We will also have to change the material we use for our [cylinder]({{< absURL "/assets/threejs/basic" >}}). [`MeshBasicMaterial`](https://threejs.org/docs/?q=material#Reference/Materials/MeshBasicMaterial) does not interact with lights at all. That means two things:

- meshes that have `MeshBasicMaterial` applied to them will always be visible
- those meshes will look like [flat pancakes](https://threejs.org/docs/scenes/material-browser.html#MeshBasicMaterial) with no 3D depth visible on them at all

So, instead of:

```javascript
material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } )
```

we'll use this:

```javascript
material = new THREE.MeshPhongMaterial( { color: 0xff3300, specular: 0x555555, shininess: 30 } )
```

We are using [`MeshPhongMaterial`](https://threejs.org/docs/?q=material#Reference/Materials/MeshPhongMaterial), which interacts with light and creates a shiny surfaces with speculiar highlights. In other words: [it has the bling](https://threejs.org/docs/scenes/material-browser.html#MeshPhongMaterial)!

You can do a lot with this material, and for our tutorial we will be using these three basic things:

- the basic material colors
- specular color (color of the reflected light)
- shininess of the specular color (how much light is reflected, higher value gives sharper reflection)


## See it in action

You can use and change the HTML document from previous part, or you can copy content of that one into a new file and add changes to original javascript code:

```javascript
// SCENE
//
scene = new THREE.Scene();

// CAMERA
//
camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 100 );
camera.position.z = 5;
scene.add( camera );

// MESH
//
geometry = new THREE.CylinderGeometry( 1, 1, 5, 16 );
// NEW: using MeshPhongMaterial now
material = new THREE.MeshPhongMaterial( { color: 0xff3300, specular: 0x555555, shininess: 30 } )
mesh = new THREE.Mesh( geometry, material );
scene.add( mesh );

// NEW: added a source of ambient light
// LIGHT: AMBIENT
//
// rgbColor, intensity
var light = new THREE.AmbientLight( 0x00ff00 ); // intensity is max if we leave it out
scene.add( light );

// RENDERER
//
renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// DO THE WORK
//
renderer.render( scene, camera );
```

it should look something like this:

{{< image src="/images/threejs/lights.png" >}}

Wait, where's the bling you talked about? This looks really dull!


## The missing *bling*: directional lights

First, let's try a little experiment. Comment out this line in your code:

```javascript
scene.add( light );
```

like so:

```javascript
// scene.add( light );
```

and reload the page in browser.

You should se a blank black page. We turned off the only light in there, the ambient light, and the scene went to dark.


### Directional lights

Ambient light has no direction, it comes from everywhere and goes everywhere, so it can't cast shadows or, for that matter, create reflections on any materials (like our MeshPhongMaterial).

Directional lights in general have a direction: they are coming from somewhere and going to somewhere else. Directional lights can cast shadows.

We can have multiple diractional lights in our scene and end up with parts of objects, or entire objects, in dark shadows where they are hardly visible or not visible at all.

Ambient lights are good for these kind of situations where they help bring those objects out of the shadows.

To illustrate all this, let's keep our ambient light off (by having the `scene.add( light )` line commented out ) and create one diractional light. Add this code above (or below) the existing code that creates the ambient light:


```javascript
// bright white
var dirLight = new THREE.DirectionalLight(0xffffff, 1);
// 45 degrees to the camera (x, y = 100, 100), and a little bit above horizontal plane (z = 50)
dirLight.position.set(100, 100, 50);
scene.add(dirLight);
```

Now if you refresh the page in browser you should see something like this:

{{< image src="/images/threejs/lights-bling.png" >}}

Whoa, [Nelly](https://en.wikipedia.org/wiki/Whoa,_Nelly!)! Shiny bling!


### Ambient lights to bring things out to, ehm, light

You'll notice how left side of the cylinder is practically invisible.

Easiest way to bring it back is to add some ambient light. We'll do that by simply uncommenting the line:

```javascript
// scene.add( light );
```

Reload the page and you should see this:

{{< image src="/images/threejs/lights-bling-ambient.png" >}}

Now you see the dark side of the cylinder (ha!) but it looks yucky and ugly, eh?

That's because ambient light has one color, directional light has another and material has base color plus color of the reflected light. All that mixes up in various places in various ways and you get this wonderful pukey reddish-greenish cylinder.

Greenish shade on the left side comes from our ambient light: we did set it's color to 0x00ff00, which is pure green color (R G B -> 00 FF 00). Play with that and change it to see what effects you get. For example try red (0xff0000) for ambient color instead:

```javascript
var light = new THREE.AmbientLight( 0xff0000 );
```

Not as good as you may have been expecting it to be, hm?

Check it out live [here]({{< ref "assets/threejs/lights.html" >}})

---

&#x25C0;
[Part 01]({{< ref "threejs/01-the-basic-stuff.md" >}})
&nbsp;
[Part 03]({{< ref "threejs/03-movement.md" >}})
&#x25BA;

