+++
date = "2017-03-30T21:13:15-05:00"
title = "03 - Movement"
topics = [ "threejs" ]
subtopic = "basics"

+++


## Let's rotate!

So far, we have a scene, a cylinder (a mesh), camera, some lights and renderer set up. It all works very well since we can see [a nicely lit cylinder]({{< ref "assets/threejs/lights.html" >}}) when we open the page in browser.

Now let's add some movement to it. Easiest thing to handle will be to rotate the mesh, since in that case we don't need to worry about cylinder going out of view and disappearing off the screen.

It would be hardly noticeable to see the rotation if we would rotate the cylinder around it's height, so we'll rotate it around one of it's perpendicular axes.

Every object in the scene (including camera or cameras, if you have more than one), has a property that contains it's position in the scene world coordinates. This property is called `position` and is a vector object with three properties: `x`, `y` and `z`. These represent coordinates of the mesh origin. Position of every point in the mesh is then expressed relative to this origin.

Next important property, when it comes to positioning of an object in a scene, is called `rotation`. This one also contains `x`, `y` and `z` properties. Values assigned to these properties represent angles in radians. These angles are rotations around **X**, **Y** and **Z** axes of the coordinate system which origin is identical to the object's origin (defined by the object's property `position`). As previously said, all points that the object consists of have their coordinates expressed relative to object's origin, that is their coordinates are expressed relative to the object's coordinate system.

Ok, enough talk, let's rotate something! The... cylinder...

By default, when created, a camera is positioned at the scene coordinate system origin (0, 0, 0) and is looking straigh down the Z axis, in the negative direction. That is why we had:

```javascript
camera.position.z = 5
```

in our code thus far. This line of code moved camera 5 units up the Z axis.

Any new object when created is placed at the origin (0, 0, 0), so when we created our cylinder it was placed at the origin. Since we moved camera away 5 units along the Z axis, and since camera looks down the Z axis (from positive towards negative values), this effectively placed the newly created cylinder right in front of our camera.

So, as you are looking at the cylinder on the screen, Z axis goes from the screen towards you, X axis goes from left to right and Y axis from bottom towards the top of the screen. Now, if we rotate the cylinder around it's Z axis, we should see it making nice circles right in front of us.

To rotate or move a mesh, just change it's `x, y, z` properties of `rotation` and/or `position` properties. To rotate our cylinder, we will simply add, i.e. 1.0 to `rotation.z`:

```javascript
mesh.rotation.z += 1.0
```

Add this line before the

```javascript
renderer.render( scene, camera )
```

line and reload the page in your browser.

Since 1.0 arc radian is about 57.0 degrees, you should see the cylinder rotated to the left for what we could eyeball as 60 degrees (actually it's about 57.29577951... degrees, but who's counting, eh? :) )

{{< image src="/images/threejs/movement-rotate.png" >}}


## Animation and the loop

Now. This is all fine and dandy so far, but how do we animate the thing, make it rotate all around?

For that, obviously, we need to change, for example, our cylinder's `rotation.z` value at regular intervals. we could make a for-loop or while-loop or whatever similar and add some small increment to `rotation.z` inside it and then call `renderer.render()` over and over again, i.e.:

```javascript
while( true )
{
    rotation.z += 0.1
    renderer.render( ... )
}
```

While it's possible, that is not the best way for various reasons. Threejs offers different mechanism that provides the best performance, and some other advantages.

The basic concept is that we need to request an **animation frame** from the threejs engine. Once the time comes for the *animation frame* to be executed, threejs will call javascript function that we provided as a parameter when we requested the *animation frame* from threejs, i.e.

```javascript
requestAnimationFrame( animate )
```

in this case, `animate` is the name of our function which will be called by threejs when the correct time comes to execute next animation frame.

So, we will write our `animate()` function to update `rotation.z` value and to render our scene:

```javascript
function animation()
{
    mesh.rotation.z += 0.1
    renderer.render( scene, camera )
}
```

We will be changing Z axis rotation angle for about 6 degrees every animation frame by adding 0.1 arc radian to it.

Replace the code

```javascript
mesh.rotation.z += 1.0

renderer.render( scene, camera )
```

in your file with this one:


```javascript
function animate()
{
    mesh.rotation.z += 0.1
    renderer.render( scene, camera )
}

requestAnimationFrame( animate )
```

If you refresh the page in your browser, you'll see the cylinder slanted to the left for about 6 degrees and.... it's still standing still!! Standing!! Still...

The reason for this is that we requested only **one** animation frame from the threejs engine. In order for this animate() function to be called again and again and again, we have to request next animation frame once we have finished with our code in `animate()` function. Add that call to the end of `animate()`:

```javascript
function animate()
{
    mesh.rotation.z += 0.1
    renderer.render( scene, camera )
    requestAnimationFrame( animate )
}

requestAnimationFrame( animate )
```

and, voila! The cylinder is rotating!

Woooohoooo!!!

Live show [here]({{< ref "assets/threejs/rotation.html" >}}).

And to play with it now, go to {{< jsfiddle "https://jsfiddle.net/zagortenej/nswnw7an/" >}}.

## A slightly different way of animating

Since this `animate->requestAnimationFrame` loop is so common in threejs, you will very often see something like this:


```javascript
function animate()
{
    requestAnimationFrame( animate )
    render()
}

animate()
```

In this case, we will define `render()` function somewhere else and put all our code in there. `animate()` will then call `render()` every frame, and will also request next animation frame from threejs. All we have to do is to call `animate()` once after we have finished all the initialization, i.e. you can replace code from previous paragraph with this one:


```javascript
function render()
{
    mesh.rotation.z += 0.02

    renderer.render( scene, camera )
}

function animate()
{
    requestAnimationFrame( animate )
    render()
}

animate()
```

and it should work just the same, but this time you will have render() function that is dedicated only to calculating the next frame.

Also, you can slow down a frentic cylinder rotation a bit and have it gracefully dance in that circle by adding 0.02 to rotation.z instead of 0.1.

There.


---

&#x25C0;
[Part 02]({{< ref "threejs/02-let-there-be-lights.md" >}})
&nbsp;
[Part 04 (TBD)]({{< ref "threejs/03-movement.md" >}})
&#x25BA;
