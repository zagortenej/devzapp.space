+++
date = "2017-03-30T15:13:51-05:00"
title = "Threejs Thingies"

+++
Threejs stuff!


<style type="text/css">
canvas {
  width: 100vw;
  height: 100vh;
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  z-index: -9999;
}
</style>

<canvas></canvas>

<script type="text/javascript" src="{{< absURL "js/threejs/three.min.js" >}}"></script>

<script type="text/javascript">
    var positions = undefined
    var colors = undefined
    var buffer_geometry = undefined
    var lines = 1000
    var mesh = undefined
    var points_group = undefined

    var canvas = document.querySelector("canvas")

    var aspect = canvas.width / canvas.height

    var scene = new THREE.Scene()
    var camera = new THREE.PerspectiveCamera(75, aspect, 0.1, 1000)
    this.camera.translateZ( 600 )

    var renderer = new THREE.WebGLRenderer({canvas: canvas})
    renderer.setClearColor(0xF0F0F0)

    function sprite_material()
    {
        // sprites for the points are squares
        // canvas size should be power of 2
        var spriteSize = 32
        
        var canvas = document.createElement('canvas');
        canvas.style.width = canvas.width = spriteSize
        canvas.style.height = canvas.height = spriteSize
        var context = canvas.getContext('2d');

        var PI2 = Math.PI * 2
        var backgroundColor = new THREE.Color(0x76b900)

        context.strokeStyle = backgroundColor.getStyle()
        context.fillStyle = backgroundColor.getStyle()
        context.beginPath()
        context.arc( spriteSize / 2, spriteSize / 2, spriteSize / 2, 0, PI2, true )
        context.fill()

        var texture = new THREE.Texture(canvas) 
        texture.needsUpdate = true

        var material = new THREE.SpriteMaterial({ map: texture })

        return material
    }

    function simple_lines()
    {
        var segments = 30
        
        // POINTS
        //
        var PI2 = Math.PI * 2
        var point_material = sprite_material()
        // Using group so we can rotate all the points at once
        // with few lines of code. See render() function.
        points_group = new THREE.Group()

        // LINES
        //
        var geometry = new THREE.BufferGeometry()
        var material = new THREE.LineBasicMaterial({ vertexColors: THREE.VertexColors })
        
        var positions = new Float32Array( segments * 3 )
        var colors = new Float32Array( segments * 3 )
        
        var r = 800
        for ( var i = 0; i < segments; i ++ )
        {
            var x = Math.random() * r - r / 2
            var y = Math.random() * r - r / 2
            var z = Math.random() * r - r / 2

            // LINE
            //
            // positions
            positions[ i * 3 ] = x
            positions[ i * 3 + 1 ] = y
            positions[ i * 3 + 2 ] = z
            // colors
            colors[ i * 3 ] = ( x / r ) + 0.5
            colors[ i * 3 + 1 ] = ( y / r ) + 0.5
            colors[ i * 3 + 2 ] = ( z / r ) + 0.5

            // POINTS
            //
            // Position points where line vertices are
            var point = new THREE.Sprite( point_material )
            point.position.x = x
            point.position.y = y
            point.position.z = z
            point.scale.x = point.scale.y = 5

            points_group.add( point )
        }

        scene.add( points_group )
        
        geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) )
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) )
        geometry.computeBoundingSphere()
        
        mesh = new THREE.Line( geometry, material )
        
        scene.add( mesh )
    }

    function onWindowResize()
    {
        var width = canvas.clientWidth
        var height = canvas.clientHeight
        if (width != canvas.width || height != canvas.height)
        {
            renderer.setSize(width, height, false)
            camera.aspect = width / height
            camera.updateProjectionMatrix()
        }
    }

    function render(time)
    {
        var time = Date.now() * 0.001
        points_group.rotation.x = mesh.rotation.x = time * 0.025
        points_group.rotation.y = mesh.rotation.y = time * 0.05

    	renderer.render(scene, camera)
    	requestAnimationFrame(render)
    }

    window.addEventListener( 'resize', onWindowResize, false )
    
    simple_lines()
    onWindowResize()
    render()
</script>

