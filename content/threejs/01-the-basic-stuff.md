+++
date = "2017-03-30T18:37:12-05:00"
title = "01 - The Basic Stuff"
topics = [ "threejs" ]
subtopic = "basics"

+++

## Basic stuff that 3D is made of in threejs

You have to create at minimum the basic stuff (tm) for threejs to show anything on your screen:

- a scene
- a camera
- an object (a mesh) in the scene for camera to look at
- a renderer to render to your screen whatever it is that the camera is looking at

### The scene

```javascript
scene = new THREE.Scene();
```

and that's it for the scene.


### The camera

There are different types of cameras, the one we need for standard 3D view is `PerspectiveCamera`.

Camera needs a few parameters:

- field of view angle
- aspect ratio of the field of view
- near and far distances

See more about all of those [here](https://en.wikipedia.org/wiki/Viewing_frustum).

```javascript
// FoV angle, aspect ratio, near frustum plane, far frustum plane
camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 100 );
camera.position.z = 5;
scene.add( camera );
```

### The stuff in the scene ( a.k.a. the mesh(es) )

Scene contains stuff called 'a mesh'. Every object you add to threejs scene is a mesh. There can be thousands of these meshes
floating around your scene and showing up on user's screen.

A Mesh has:

- geometry
- material

In general, a geometry consists of points connected by lines in a way that all points and lines create triangles.

Example of a mesh which kind of looks like a cylinder:

{{< image src="/images/threejs/threejs-mesh.png" >}}

There are special kinds of geometries where you can only have lines, without them forming trinagles or any shapes like that.
Those are called, well, lines and not meshes, and are used to draw, you know, lines. We'll work with meshes for now.

You can build a geometry yourself point by point and line by line, and/or you can use helper methods
like [`CylinderGeometry`](https://threejs.org/docs/?q=geometry#Reference/Geometries/CylinderGeometry), [`PlaneGeometry`](https://threejs.org/docs/?q=geometry#Reference/Geometries/PlaneGeometry), [`BoxGeometry`](https://threejs.org/docs/?q=geometry#Reference/Geometries/BoxGeometry) etc.

```javascript
// rTop, rBottom, height, nSegments
geometry = new THREE.CylinderGeometry( 1, 1, 5, 16 );
```

You can also build a material from ground up and/or you can use helper methods like
[`MeshBasicMaterial`](https://threejs.org/docs/?q=material#Reference/Materials/MeshBasicMaterial), [`LineBasicMaterial`](https://threejs.org/docs/?q=material#Reference/Materials/LineBasicMaterial) etc.

```javascript
material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );
```

After you've got geometry and material made, creating the mesh is simple:

```javascript
mesh = new THREE.Mesh( geometry, material );
scene.add( mesh );
```

Always add your mesh to your scene!

You can also add one mesh to another mesh or create a group and add meshes to it etc. but for simplicity sake we will just add everything directly to the scene for now.


### The renderer

Renderer renders the scene as seen by the camera.

In very general case, the scene is rendered on a rectangular plane also known as your monitor. Usually, it is just a part of your monitor occupied by browser window, or rather a piece of the browser window.

There are different types of renderers and the best one to use would be the [`WebGLRenderer`](https://threejs.org/docs/?q=renderer#Reference/Renderers/WebGLRenderer), since it will use whatever graphics card your user has in order to speed things up when drawing (rendering) the scene.

The most basic thing to do when creating a renderer is to give it width and height in pixels of the area where the camera view of the scene is to be rendered:

```javascript
renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
```

Now we need to tell renderer where to place the result of its work.


### The place to render to

At this point, we dive into the browser's internal world of DOM objects, and attach the renderer to a specific place in the browser document.

The simplest thing to do is to use the entire browser's document view for our renderer, which means we will tell renderer to use a space in the browser's window occupied by the `<body>` DOM element:

```javascript
document.body.appendChild( renderer.domElement );
```

At the end, we need to tell the renderer to do it's job, and that is to render the scene as viewed by the camera onto the area occupied by `<body>` element. This is done by calling:

```javascript
renderer.render( scene, camera );
```

Now we only need to make sure that `<body>` element will take up the entire document window in the browser. Easiest way to do it is i.e. like this:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8>
        <title>My first three.js app</title>
        <style>
            body { margin: 0; }
            canvas { width: 100%; height: 100% }
        </style>
    </head>
    <body>
        <!-- Threejs renderer will render here whatever camera sees in the scene -->
    </body>
</html>
```

This is the most elementary HTML document to be used with threejs. We only need two more things:

- threejs library itself
- a place to write our own code

We add these using `<script>` tags, one to load threejs and the other to write our own code into:


```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/84/three.min.js"></script>
<script>
    // Our Javascript will go here.
</script>
```

> NOTE: We are using [cdnjs](https://cdnjs.cloudflare.com) to pull in
> a specific version of threejs library into our simple threejs application.
> In this example, the version is **84**. Change the number in above link to the version
> you would like to use in your code.

So the entire document looks like this:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8>
        <title>My first three.js app</title>
        <style>
            body { margin: 0; }
            canvas { width: 100%; height: 100% }
        </style>
    </head>
    <body>
        <!-- Threejs renderer will render here whatever camera sees in the scene -->
    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/84/three.min.js"></script>
    <script>
        // Our Javascript will go here.
    </script>

</html>
```

Now, replace the line `// Our Javascript will go here.` with this:

```javascript
// SCENE
//
scene = new THREE.Scene();

// CAMERA
//
camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 100 );
camera.position.z = 5;
scene.add( camera );

// MESH
//
geometry = new THREE.CylinderGeometry( 1, 1, 5, 16 );
material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );
mesh = new THREE.Mesh( geometry, material );
scene.add( mesh );

// RENDERER
//
renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// DO THE WORK
//
renderer.render( scene, camera );
```

and that's it.



## Viewing the thingie

Let's see what we've created.

Create a HTML document with this content:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8>
        <title>My first three.js app</title>
        <style>
            body { margin: 0; }
            canvas { width: 100%; height: 100% }
        </style>
    </head>
    <body>
        <!-- Threejs renderer will render here whatever camera sees in the scene -->
    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/84/three.min.js"></script>
    <script>
        // SCENE
        //
        scene = new THREE.Scene();

        // CAMERA
        //
        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 100 );
        camera.position.z = 5;
        scene.add( camera );

        // MESH
        //
        geometry = new THREE.CylinderGeometry( 1, 1, 5, 16 );
        material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );
        mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        // RENDERER
        //
        renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( renderer.domElement );

        // DO THE WORK
        //
        renderer.render( scene, camera );
    </script>

</html>
```

and save it in a folder somewhere.

Open the page in your browser and you should see a mesh which has roughly a shape of a cylinder:

{{< image src="/images/threejs/basic.png" >}}

Notice how all points and lines in the cylinder mesh are connected in a way that they form triangles.

Check it out live [here]({{< ref "assets/threejs/basic.html" >}}).

Play with it in {{< jsfiddle "https://jsfiddle.net/zagortenej/nswnw7an/" >}}.


## What happend with lights?

Some (or many) of you may ask: where are the lights? Every scene has to have lights, right?

Well, keeping things as basic as possible is the aim of these tutorials.

And you don't have to have lights in order to see anything rendered, provided you use `MeshBasicMaterial` when creating your
material. This material, and i.e. `LineBasicMaterial`, is not affected by lights. Not being affected by lights, `MeshBasicMaterial` is not really well suited for rendering 3D objects, since you only see flat shapes (unless you add texture to it, but hey, this is a 'basic' tutorial, remember?).

To allevieate this a bit we've set `wireframe: true` on the material for our cylinder, so the cylinder is rendered as a wireframe object. It may not be the best thing to do for complex scenes, but for simple scenes and tutorials which are concerned with laying out the basic concepts, like this one, that is more than adequate.

And, well, there will be light in the [next part]({{< ref "threejs/02-let-there-be-lights.md" >}}) anyways. :)

---

[Part 02]({{< ref "threejs/02-let-there-be-lights.md" >}})
&#x25BA;

